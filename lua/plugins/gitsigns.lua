return {
    'lewis6991/gitsigns.nvim',
    version = "*",
    config = function()
        require('gitsigns').setup({})

        -- Correction de GitSigns qui surligne ses symboles
        vim.api.nvim_set_hl(0, "Signcolumn", { ctermbg=NONE })
        vim.cmd([[hi DiffAdd ctermfg=Green ctermbg=NONE]])
        vim.cmd([[hi DiffChange ctermfg=Yellow ctermbg=NONE]])
        vim.cmd([[hi DiffDelete ctermfg=Red ctermbg=NONE]])

    end
}
