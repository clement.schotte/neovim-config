return {
    {
        "sainnhe/everforest",
        config = function()
            --vim.cmd.colorscheme "everforest"
            -- Remettre de la transparence partout
            vim.api.nvim_set_hl(0, "Normal", { ctermfg=NONE,  ctermbg=NONE })
            --vim.api.nvim_set_hl(0, "LineNr", { ctermfg=NONE,  ctermbg=NONE })
            --vim.api.nvim_set_hl(0, "SignColumn", { ctermfg=NONE,  ctermbg=NONE })
            --vim.api.nvim_set_hl(0, "EndOfBuffer", { ctermfg=NONE,  ctermbg=NONE })
        end
    },

    {
        "owickstrom/vim-colors-paramount"
    },

    {
        "catppuccin/nvim",
        name = "catppuccin",
        priority = 1000,
        config = function()
            require("catppuccin").setup({
                transparent_background = false
            })
            --vim.cmd.colorscheme "catppuccin"
            ---- Remettre de la transparence partout
            --vim.api.nvim_set_hl(0, "Normal", { ctermfg=NONE,  ctermbg=NONE })
        end,
    },

    {
        'mhartington/oceanic-next',
        priority = 1000,
        config = function()
            --vim.cmd.colorscheme "OceanicNext"
            -- Remettre de la transparence partout
            vim.api.nvim_set_hl(0, "Normal", { ctermfg=NONE,  ctermbg=NONE })
            --vim.api.nvim_set_hl(0, "LineNr", { ctermfg=NONE,  ctermbg=NONE })
            --vim.api.nvim_set_hl(0, "SignColumn", { ctermfg=NONE,  ctermbg=NONE })
            --vim.api.nvim_set_hl(0, "EndOfBuffer", { ctermfg=NONE,  ctermbg=NONE })
        end
    }
}
