-- Avec la très nécessaire aide de https://invidious.fdn.fr/watch?v=S-xzYgTLVJE&listen=false
return {
    {
        "williamboman/mason.nvim",
        config = function()
            require("mason").setup()
        end
    },
    {
        "williamboman/mason-lspconfig.nvim",
        config = function()
            require("mason-lspconfig").setup({
                ensure_installed = { "lua_ls", "clangd", "rust_analyzer", "pylsp" }
            })
        end
    },

    {
        "neovim/nvim-lspconfig",
        config = function()
            local lspconfig = require("lspconfig")

            -- Language Servers
            lspconfig.lua_ls.setup({})
            lspconfig.clangd.setup({})
            lspconfig.rust_analyzer.setup({})
            lspconfig.pylsp.setup({})

            -- Mappings
            vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
            vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)

            --Scripts venus de https://github.com/neovim/nvim-lspconfig/wiki/UI-customization

            -- Configuration
            vim.diagnostic.config({
                virtual_text = false,
                signs = true,
                underline = true,
                update_in_insert = false,
                severity_sort = true,
            })

            -- Afficher les erreurs dans des info-bulles
            -- You will likely want to reduce updatetime which affects CursorHold
            -- note: this setting is global and should be set only once
            vim.o.updatetime = 500
            vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
                group = vim.api.nvim_create_augroup("float_diagnostic", { clear = true }),
                callback = function ()
                    vim.diagnostic.open_float(nil, {focus=false})
                end
            })

            -- Changer les icônes de la goutière
            local signs = { Error = "󰅚 ", Warn = "󰀪 ", Hint = "󰌶 ", Info = " " }
            for type, icon in pairs(signs) do
                local hl = "DiagnosticSign" .. type
                vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
            end


            -- Montrer uniquement l'erreur la plus élevée
            -- Create a custom namespace. This will aggregate signs from all other
            -- namespaces and only show the one with the highest severity on a
            -- given line
            local ns = vim.api.nvim_create_namespace("my_namespace")

            -- Get a reference to the original signs handler
            local orig_signs_handler = vim.diagnostic.handlers.signs

            -- Override the built-in signs handler
            vim.diagnostic.handlers.signs = {
              show = function(_, bufnr, _, opts)
                -- Get all diagnostics from the whole buffer rather than just the
                -- diagnostics passed to the handler
                local diagnostics = vim.diagnostic.get(bufnr)

                -- Find the "worst" diagnostic per line
                local max_severity_per_line = {}
                for _, d in pairs(diagnostics) do
                  local m = max_severity_per_line[d.lnum]
                  if not m or d.severity < m.severity then
                    max_severity_per_line[d.lnum] = d
                  end
                end

                -- Pass the filtered diagnostics (with our custom namespace) to
                -- the original handler
                local filtered_diagnostics = vim.tbl_values(max_severity_per_line)
                orig_signs_handler.show(ns, bufnr, filtered_diagnostics, opts)
              end,
              hide = function(_, bufnr)
                orig_signs_handler.hide(ns, bufnr)
              end,
            }

        end
    }
}
