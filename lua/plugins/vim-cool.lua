return {
    -- Plugin pour éteindre automatiquement le surlignage des recherches
    -- (au lieu d'utiliser la commande :noh ou Ctrl+l)
    "romainl/vim-cool"
}
