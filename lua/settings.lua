local opt = vim.opt

-- Numéros de ligne
opt.number = true

-- Tabs de 4 espaces
opt.shiftwidth = 4
opt.smarttab = true
opt.expandtab = true

-- Partage du presse-papier avec le système
opt.clipboard = 'unnamedplus'

-- Considérer les fichiers .i comme du C
vim.cmd([[autocmd BufRead,BufNewFile *.i setfiletype c]])

-- Dictionnaire en français
opt.spelllang='fr'
-- Activer avec set spell
-- z= pour voir les suggestions

-- Se rappeler de la position du curseur à l'ouverture d'un fichier
vim.api.nvim_create_autocmd({'BufWinEnter'}, {
    desc = 'return cursor to where it was last time closing the file',
    pattern = '*',
    command = 'silent! normal! g`"zv',
})

-- Afficher les caractères qui dépassent 80 colonnes comme des erreurs
vim.cmd([[au BufWinEnter *.py,*.i*,.jl,*.c let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)]])

-- Afficher les indentations en tab comme des erreurs
vim.cmd([[match ErrorMsg /^\t\+/]])

-- Permettre d'afficher GitSigns et LSP en même temps
opt.signcolumn = "auto:2"

-- Garder les actions annulées entre les sessions
opt.undofile = true

-- Changer la leader key en Espace (\ par défaut)
vim.g.mapleader = ' '

-- Ne pas sauter au prochain mot lorsqu'on utilise *
local function shine_forward()
    vim.fn.setreg("/", "\\<" .. vim.fn.expand("<cword>") .. "\\>")
    vim.opt.hlsearch = true
end
vim.keymap.set('n', '*', shine_forward, { silent = true, noremap = true })
